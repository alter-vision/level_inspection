# Fill level inspection

Esse projeto é referente a inspeção de nivel de líquidos em garrafas plástica.
Projeto de Diplomação: Julio Milani de Lucena 
Engenharia Elétrica - UFRGS 2020

![](out/sample.png)

## Getting Started
```
$ git clone https://gitlab.com/alter-vision/level_inspection.git
$ cd level_inspection

#instala todos os pacotes necessários
$ pip install requirements.txt
$ streamlit run st_app.py
```

## Authors

* **Julio Milani de Lucena** - UFRGS - julio.lucena@ufrgs.br
Alter Vision::Visão Inteligente
www.altervision.com.br 